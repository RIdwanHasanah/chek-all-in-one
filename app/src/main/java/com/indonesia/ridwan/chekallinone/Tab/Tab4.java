package com.indonesia.ridwan.chekallinone.Tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indonesia.ridwan.chekallinone.R;

/**
 * Created by hasanah on 8/14/16.
 */
public class Tab4 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return  inflater.inflate(R.layout.tab4,container,false);
    }
}

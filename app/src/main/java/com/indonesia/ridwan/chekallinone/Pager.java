package com.indonesia.ridwan.chekallinone;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.indonesia.ridwan.chekallinone.Tab.Tab1;
import com.indonesia.ridwan.chekallinone.Tab.Tab2;
import com.indonesia.ridwan.chekallinone.Tab.Tab3;
import com.indonesia.ridwan.chekallinone.Tab.Tab4;
import com.indonesia.ridwan.chekallinone.Tab.Tab5;
import com.indonesia.ridwan.chekallinone.Tab.Tab6;
import com.indonesia.ridwan.chekallinone.Tab.Tab7;

/**
 * Created by hasanah on 8/14/16.
 */
public class Pager extends FragmentStatePagerAdapter {

    int tabCount;
    public Pager(FragmentManager fm, int tabCount){
        super(fm);
        this.tabCount=tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                Tab1 tab1 = new Tab1();
                return tab1;
            case 1:
                Tab2 tab2 = new Tab2();
                return tab2;
            case 2:
                Tab3 tab3 = new Tab3();
                return tab3;
            case 3:
                Tab4 tab4 = new Tab4();
                return tab4;
            case 4:
                Tab5 tab5 = new Tab5();
                return tab5;
            case 5:
                Tab6 tab6 = new Tab6();
                return tab6;
            case 6:
                Tab7 tab7 = new Tab7();
                return tab7;
            default:
                return null;
        }
    }

    public int getCount() {
        return tabCount;
    }
}
